import React, {createContext, Component} from 'react';
import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/analytics";

export const FirebaseContext = createContext();

const firebaseConfig = {
  apiKey: "AIzaSyAoOildIL4wtL1ObsWunIZ3I5aQ8Tdly_s",
  authDomain: "online-scratchit.firebaseapp.com",
  projectId: "online-scratchit",
  storageBucket: "online-scratchit.appspot.com",
  messagingSenderId: "572754168808",
  appId: "1:572754168808:web:f8942bd5327ae5d21df25d",
  measurementId: "G-P05TTY9VRK"
  };

  class FirebaseContextProvider extends Component {
    constructor(props){
    super(props);
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
    this.auth = firebase.auth();
    this.store = firebase.firestore();
    this.functions = firebase.functions();
    this.analytics=firebase.analytics();
    this.fieldValue = firebase.firestore.FieldValue;
    this.timestamp = firebase.firestore.Timestamp;

    this.state= {
        status: false,
        loading: false,
        redirect: false
    }

    }

    watch = ()=>{
        this.auth.onAuthStateChanged(
        (user) => {
        if (user) {  
            this.setState( {status : true});
            localStorage.setItem("status",true);
        }
        else {
            this.setState( {status : false});
        }
         });
    }   

    signin= async () =>{
        var provider = new firebase.auth.FacebookAuthProvider();
        await firebase.auth().signInWithRedirect(provider);
        this.setState({status:true, redirect:true});
        localStorage.setItem("status",true);


    }

    signout= async() =>{
        this.setState({
            status: false,
        });
        this.auth.signOut();
        localStorage.clear();
        sessionStorage.clear();
        console.log("User signs out");
        }


    render(){
        return(
            <FirebaseContext.Provider value={{...this.state,Timestamp:this.timestamp, fieldValue:this.fieldValue,serverTimestamp:this.serverTimestamp,arrayRemove:this.arrayRemove,arrayUnion:this.arrayUnion,setUid:this.setUid,tutorset:this.tutorset,modset:this.modset,adminset:this.adminset, auth:this.auth,functions:this.functions,store: this.store, signout:this.signout,watch:this.watch,signin:this.signin}}>
                {this.props.children}
            </FirebaseContext.Provider>
        );
    }
}


export default FirebaseContextProvider;