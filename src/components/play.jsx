import React, {useEffect,useContext, useState, useRef} from 'react';
import { FirebaseContext } from '../contexts/firebase';
import "../styles/all.css";
import Pay from './pay.jsx';

import redhot from '../assets/redhot.jpg';
import bananas from '../assets/bananas.jpg';
import lucky from '../assets/lucky.jpg';
import moneytree from '../assets/moneytree.jpg';
import golds from '../assets/golds.jpg';
import cash from '../assets/cash.jpg';
import goldm from '../assets/goldm.jpg';
import goldl from '../assets/goldl.jpg';
import gcash from '../assets/gcash.png';
//import { YoutubeSearchedForTwoTone } from '@material-ui/icons';

/**
 * table contents:
 * Pic, Name, Price, Quantity (HM Left)
 * Buy now
 * 
 * todo:
 * overlay for totals *done*
 * paywall *done*
 * card animations *in process*
 *      scratch outer layer
 *      banana
 *          random values, define only number that will be three (grid) and multiplier
 *      redhot
 *          random values, define only direction of digits
 *      money tree
 *          random icons, define only number of icons based on payout
 *      lucky sheep
 *          random values, define only number that will be three and bonus
 *      go for gold
 *          random values, define only payout and multiplier
 *      ----
 *      extra
 *          random values, define only payout and multiplier
 *      go for gold
 *          random values, define only payout and multiplier
 *      go for gold
 *          random values, define only payout and multiplier
 * payout summary *done*
 * burger menu (Account name, wallet balance, play, history) *done*
 * load 
 * withdraw
 * logic with useeffect+tesseract+firebase
 * footer
 * landing
 */





function Play(){
    const [display, setDisplay] = useState(0);
    
    //games directory
    const games = [
        {"name":"Go Bananas!",
        "price":"P20",
        "img":bananas,
        "color":"yellow",
        "qty":0
        },
        {"name":"Red Hot 7s",
        "price":"P20",
        "img":redhot,
        "color":"orange",
        "qty":0
        },
        {"name":"Money Tree",
        "price":"P20",
        "img":moneytree,
        "color":"skyblue",
        "qty":0
        },
        {"name":"Lucky Sheep",
        "price":"P20",
        "img":lucky,
        "color":"red",
        "qty":0
        },
        {"name":"Go for Gold! (S)",
        "price":"P20",
        "img":golds,
        "color":"navyblue",
        "qty":0
        },
        {"name":"Cash Extravaganza",
        "price":"P50",
        "img":cash,
        "color":"pink",
        "qty":0
        },
        {"name":"Go for Gold! (M)",
        "price":"P50",
        "img":goldm,
        "color":"navyblue",
        "qty":0
        },
        {"name":"Go for Gold! (L)",
        "price":"P100",
        "img":goldl,
        "color":"navyblue",
        "qty":0
        },
    ]

    const [buy,setBuy]=useState(games);

    //counter
    const choices = games.map((element,i)=>
    <tr key={element.name} className="play-buycard-wrapper">
        <td className={`play-buycard-card ${element.color}`}>
            <div className={`play-buycard-card-content`}>
                <div className="play-buycard-card-title">
                    <span id="card-name">{element.name}</span> <span id="price">{element.price}</span>
                </div>
                <img alt="scratch card" src={element.img}/>
            </div>
        </td>
        <td className="play-buycard-counter">
            <div onClick={()=>{
                let array = [...buy];
                array[i].qty+=1;
                setBuy(array);
                }} 
                className="play-buycard-counter-plus">+</div>
            <div className="play-buycard-counter-num">{buy[i].qty}</div>
            <div onClick={()=>{
                let array = [...buy];
                array[i].qty>0?array[i].qty-=1:array[i].qty=array[i].qty; //lower limit
                setBuy(array);
                }} className="play-buycard-counter-minus">-</div>
        </td>
    </tr>
    )

    //total price
    const [totalPrice,setTotalPrice] = useState(0);

    useEffect(()=>{
        //console.log(buy);

        const price = buy.map(a=>parseInt(a.price.substring(1)));
        const qty = buy.map(a => a.qty);

        function sumProduct(array1,array2){
            let sum = 0
            for (let i =0; i<array1.length;i++){
                sum+=array1[i]*array2[i];
            }
            return sum
        }
        setTotalPrice(sumProduct(price,qty));
    },[buy])

    //const result = games.map(a => a.qty);
    //console.log("foo",result)

    if(display===1){
        return(
            <Pay gamesList={buy} totalPrice={totalPrice} showPay={value=>setDisplay(value)}/>
            )
    }

    else{
        return(
            <div className="play-new-wrapper">
                <div className="play-scratch-wrapper">
                    <div className="play-scratch-content">
                        <div className="play-scratch-title"><span id="large">₱</span>20</div>
                        <div className="play-scratch-subtitle">Latest Winner</div>
                        <div className="play-scratch-subtitle">06/23/21</div>
                    </div>
                    <div className="play-scratch-image">
                        <img id="win-scratch" alt="scratch card" src={redhot}/>
                    </div>
                </div>
                <table className="play-table">
                    <thead className="align-left">
                        <tr>
                            <th>Card</th>
                            <th>Buying</th>
                        </tr>
                        {choices}
                    </thead>
                </table>
                <div className={`play-cart-wrapper ${totalPrice>0?"":"hide"}`}>
                    <div className="play-cart-text">
                        <div className="play-cart-text-label">
                            TOTAL COST:
                        </div>
                        <div className="play-cart-text-price">
                            <span id="large">₱</span> {totalPrice}
                        </div>
                    </div>
                    <div onClick={()=>setDisplay(1)} className="play-cart-button">
                        <img id="gcash-cart" src={gcash}/><span id="gcash-label">Pay</span>
                    </div>
                </div>
            </div>
        )
    }
}

export default Play;