import pic from '../assets/trial2.png'
import play from '../assets/play-button.svg';
import { useContext, useEffect, useState } from "react";
import {Link} from 'react-router-dom';
//import { FirebaseContext } from "./contexts/firebase.jsx";
import '../styles/all.css';


import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';


function Landing() {

    //const fire = useContext(FirebaseContext);
  
    return (
      <div className="wrapper">
          <div className="container">
            <div className="title">
                {<img id="logo" alt="title" src={pic}/>}
            </div>
              <a href="/play"> 
                  <div role="button" className="btn">
                      <img src={play}/>
                  </div>
              </a>
            </div>
      </div>
    );
  }
  
  export default Landing;