import React, {useEffect,useContext, useState, useRef} from 'react';

import redhot from '../assets/redhot.jpg';
import bananas from '../assets/bananas.jpg';
import lucky from '../assets/lucky.jpg';
import moneytree from '../assets/moneytree.jpg';
import golds from '../assets/golds.jpg';
import cash from '../assets/cash.jpg';
import goldm from '../assets/goldm.jpg';
import goldl from '../assets/goldl.jpg';

import "../styles/all.css";

const result={
    table(size){
        if (size==="s"){
            return(
                <table>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>2</td>
                        <td>3</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>7</td>
                        <td>7</td>
                    </tr>
                    <tr>
                        <td className="winning-num">7</td>
                        <td>7</td>
                        <td>7</td>
                    </tr>
                </tbody>
            </table>
            )
        }

        else if (size==="m"){
            return(
                <div className="scratch-card-table-m-wrapper">
                <table className="scratch-card-table-m-winning">
                    <tr>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>1</td>
                    </tr>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>2</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td className="winning-num">7</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td className="winning-num">7</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            )
        }
        else if (size==="l"){
            return(
                <div className="scratch-card-table-l-wrapper">
                <table className="scratch-card-table-l-winning">
                    <tr>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                    </tr>
                </table>
                <table>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td className="winning-num">7</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td className="winning-num">7</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td className="winning-num">7</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                        <tr>
                            <td className="winning-num">7</td>
                            <td>7</td>
                            <td>7</td>
                            <td>2</td>
                            <td>3</td>
                            <td>3</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            )
        }
    }
}

const cards = [
    {"src":redhot,"position":"redhot-position","bonus":false,"bonus-position":"redhot-bonus","size":"s"},
    {"src":bananas,"position":"bananas-position","bonus":true,"bonus-position":"bananas-bonus","size":"s"},
    {"src":lucky,"position":"lucky-position","bonus":true,"bonus-position":"lucky-bonus","size":"s"},
    {"src":moneytree,"position":"moneytree-position","bonus":false,"bonus-position":"moneytree-bonus","size":"s"},
    {"src":golds,"position":"golds-position","bonus":true,"bonus-position":"golds-bonus","size":"s"},
    {"src":cash,"position":"cash-position","bonus":true,"bonus-position":"cash-bonus","size":"m"},
    {"src":goldm,"position":"goldm-position","bonus":true,"bonus-position":"goldm-bonus","size":"m"},
    {"src":goldl,"position":"goldl-position","bonus":true,"bonus-position":"goldl-bonus","size":"l"},
]


function Test(){



    const [num,setNum]=useState(0);

//Scratch-off Code
    const [imageNew,setImageNew]=useState(null);
    const imageRef = useRef();
    imageRef.current=imageNew;


    useEffect(()=>{
            const canvas = document.getElementById('canvas');
            const ctx = canvas.getContext('2d');

            const image = document.getElementById('SourceImage');
            image.src=bananas;

            canvas.width =  image.width; 
            canvas.height = image.height;
            ctx.drawImage(image,0,0);

            function pick(event) {
                var x = event.offsetX;
                var y = event.offsetY;
                const coord = {"x":x,"y":y}

                return coord;
            }

            function touchpick(event,rect) {
                var x = Math.ceil(event.targetTouches[0].pageX - rect.left);
                var y = Math.ceil(event.targetTouches[0].pageY - rect.top);
                const coord = {"x":x,"y":y};
                //console.log("coords",coord);

                return coord;
            }


            function transparent(x,y) {
                //console.log("xy",x,y);
                ctx.drawImage(image, 0, 0);
                const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
                //const data = imageNew?imageNew.data:imageData.data;
                //const data = imageData.data;
                const data = imageRef.current?imageRef.current.data:imageData.data;
                //console.log("data",data);

                //radius of transparency
                for (let yNew = y-25; yNew <= y; yNew += 1) {
                    for (let i = 4*(yNew*canvas.width+x)+100; i >= 4*(yNew*canvas.width+x)-100; i -= 4) {
                        data[i+3]=0;
                    }
                }

                for (let yNew = y+25; yNew >= y; yNew -= 1) {
                    for (let i = 4*(yNew*canvas.width+x)-100; i <= 4*(yNew*canvas.width+x)+100; i += 4) {
                        data[i+3]=0; 
                    }
                }
                
                //ctx.putImageData(imageData, 0, 0);

                ctx.putImageData(imageRef.current?imageRef.current:imageData, 0, 0);
                imageRef.current?setImageNew(imageRef.current):setImageNew(imageData);

                //ctx.putImageData(imageNew?imageNew:imageData, 0, 0);
                //imageNew?setImageNew(imageNew):setImageNew(imageData);  
                //console.log("image new",imageRef.current);

            };


            //Paintbrush behavior
            function mouseMoveWhilstDown(target, whileMove) {
                const endMove = function () {
                    window.removeEventListener('mousemove', whileMove);
                    window.removeEventListener('mouseup', endMove);
                };
            
                target.addEventListener('mousedown', function (event) {
                    //event.stopPropagation(); // remove if you do want it to propagate ..
                    window.addEventListener('mousemove', whileMove);
                    window.addEventListener('mouseup', endMove);   
                });

            }

            function touchMoveWhilstDown(target, whileMove) {
                const endMove = function () {
                    window.removeEventListener('touchmove', whileMove);
                    window.removeEventListener('touchend', endMove);
                };

                target.addEventListener('touchstart', function (event) {
                    //event.stopPropagation(); // remove if you do want it to propagate .
                    window.addEventListener('touchmove', whileMove,{passive:true});
                    window.addEventListener('touchend', endMove,{passive:true});   
                });
            }

            mouseMoveWhilstDown(canvas,(event)=>{ 
                transparent(pick(event).x,pick(event).y);
            });

            touchMoveWhilstDown(canvas,(event)=>{ 
                var rect = event.target.getBoundingClientRect();
                transparent(touchpick(event,rect).x,touchpick(event,rect).y);
            });

    },[document.getElementById('canvas')])
   
    return(
    <div className="relative no-scroll">
        <h1>HELLO</h1>
        <img id="SourceImage" className="hide"/>
        <canvas id="canvas"></canvas>
        <div className={`test-position scratch-content`}>
                    {result.table(cards[num].size)}
        </div>
    </div>
    )
}

export default Test;