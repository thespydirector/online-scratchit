import React, {useEffect,useContext, useState} from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
import { FirebaseContext } from '../contexts/firebase.jsx';
import Landing from './landing.jsx';
import PlayMain from './playmain.jsx';
import Loading from './loading.jsx';
import Cards from './cards.jsx';
import Test from './load.jsx';

import ScrollToTop from '../scripts/ScrollToTop.js';

function Main(){
    const fire = useContext(FirebaseContext);

    useEffect(()=>{
        fire.watch();
    },[fire])
    
    return(
        <ScrollToTop>
          <Switch>
              <Route exact path="/" component={Landing}/>
              <Route exact path="/play" component={PlayMain}/>
              <Route exact path="/loading" component={Loading}/>
              <Route exact path="/cards" component={Cards}/>
              <Route exact path="/test" component={Test}/>
          </Switch>
        </ScrollToTop>
    )
}

function PrivateRoute({ children, ...rest }) {
    const fire = useContext(FirebaseContext);

    return (
      <Route
        {...rest}
        render={({ location }) =>
          //fire.status 
          localStorage.getItem("status")? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

export default Main;