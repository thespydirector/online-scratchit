import React, {useState,useContext,useEffect,useRef} from 'react';
import Topbar from './TopbarComponent.js';
import FooterNew from './FooterNewComponent';

import{FirebaseContext} from '../contexts/FirebaseContext';
import {ClickContext} from '../contexts/ClickContext';
import {FilterContext} from '../contexts/FilterContext';

import CancelRoundedIcon from '@material-ui/icons/CancelRounded';
import IconButton from '@material-ui/core/IconButton';

import CircularProgress from '@material-ui/core/CircularProgress';


import { Link, useParams, useLocation } from 'react-router-dom';
import Latex from 'react-latex';

import './styles/forum.css';
import './styles/mydashboard.css';
import './styles/selection.css';


export function ForumNew(){
    let { id } = useParams();
    const fire = useContext(FirebaseContext);
    const [orderBy, setOrderBy]=useState("timestamp");
    const [desc, setDesc]=useState("desc");
    const [select,setSelect]=useState("Newest");


    const [question,setQuestion] = useState("");
    const [description,setDescription] = useState("");
    const inputEl = useRef();

    const [image,setImage] = useState(null);
    const [imageReset,setImageReset] = useState("");
    const [name,setName]=useState("");

    const [showDisc,setShowDisc]=useState(false);
    const [showSearch,setShowSearch]=useState(false);

    const [loading, setLoading]=useState(false);

    const [textSearch, setTextSearch]=useState("");
    const [query, setQuery]=useState([""]);

    const [prelim, setPrelim] =useState("");
    const prelimRef = useRef();
    prelimRef.current = prelim;

    const wrapperRef = useRef(null);


    useEffect(() => {
        window.addEventListener("click", handleClickOutside);
        return () => {
          window.removeEventListener("click", handleClickOutside);
        };
      });
    
      const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
                setShowSelect(false);
          //(event.target);
        }
      };   

    const imageRef = useRef();
    imageRef.current = image;

    const imagePreview = useRef({src:""});

    const [showSelect,setShowSelect]=useState(false);

    function previewFile(file) {
        const preview = imagePreview.current;
        const reader = new FileReader();
      
        reader.addEventListener("load", function () {
          // convert image file to base64 string
          preview.src = reader.result;
        }, false);
      
        if (file) {
          reader.readAsDataURL(file);
          setName(file.name);
        }
      }

      useEffect(()=>{
        sessionStorage.setItem("subjectcode",id);
    },[])

async function saveIt(){
        setLoading(true);

        if (image) {
        const files = image;
        const data = new FormData();

        data.append('file',files[0]);
        data.append('upload_preset','imgupload');
        //setLoading(true);

        const res = await fetch(
            'https://api.cloudinary.com/v1_1/scholarbuddy/image/upload',
            {
                method: 'POST',
                body: data
            }
        )
        
        const file = await res.json();
        setImage(file.secure_url);
        }

        
        const addPost = await fire.store.collection("forum").add({
            question: question,
            description: description,
            subjectCode: `${id}`,
            timestamp: fire.fieldValue.serverTimestamp(),
            img: imageRef.current,
            keywords: question[question.length-1]=="?" || question[question.length-1]=="!"||question[question.length-1]=="." ? ["",...question.substring(0,question.length-1).toLowerCase().split(" ")] : ["",...question.toLowerCase().split(" ")],
            savedBy: fire.fieldValue.arrayUnion(fire.auth.currentUser.uid),
            flaggedBy: [],
            modans: {
                text:"No leaks. Maintain respect.",
                timestamp: fire.fieldValue.serverTimestamp()
            },
            answerers:[],
            tutorans: [],
            answersLength:0,
            show: true,
            user: fire.auth.currentUser.uid,
            likes: 0
        });

        await fire.store.collection("forum").doc(addPost.id).collection("answers").add({ //c.content.answers[i].replies=[...c.content.answers[i].replies,answer],
            user: "System Moderator",
            answer: 
            `Welcome to Studybuddy, an anonymous online forum where you can discuss any subject in the Ateneo! To maintain a conducive space for constructive discussions, here are a few ground rules:
            
            1. Please respect one another. Let us help one another towards the path of learning. 
            2. Any form of academic dishonesty is not allowed. Users who leak exams/assignment answers will be banned permanently.
            3. Askers are expected to show their preliminary work.
            4. Memes and other inappropriate content are not allowed and will be removed.

            Here in Studybuddy, we study as a community. Happy learning!
            `,
            img: null,
            upvoteLength:0,
            upvotes: [],
            downvotes:[],
            timestamp: fire.fieldValue.serverTimestamp(),
            systemMod: true
        });

        await fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uQuestions: fire.fieldValue.arrayUnion(addPost.id)
        });

        await fire.store.collection("stats").doc("forum collection size").update({
            size: fire.fieldValue.increment(1)
        })

        setImage(null);
        setImageReset("");
        imagePreview.current.src = ""; 
        setQuestion("");
        setDescription("");
        /* setCode(""); */
        setLoading(false);
        /* setOpen(true); */
    } 


function searchIt (){
    //console.log("From searchIt:",...prelimRef.current.split(" "));
    setLoading(true);
    if (prelimRef.current.split(" ").length<=10){
    setQuery(
        prelimRef.current[prelimRef.current.length-1]=="?" ||
        prelimRef.current[prelimRef.current.length-1]=="!"||
        prelimRef.current[prelimRef.current.length-1]=="." ? 
        [...prelimRef.current.substring(0,prelimRef.current.length-1).toLowerCase().split(" ")] :
        [...prelimRef.current.toLowerCase().split(" ")]
        );
        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }

    else if (prelimRef.current.split(" ").length>10){
        setQuery(
            prelimRef.current[9]=="?" ||
            prelimRef.current[9]=="!"||
            prelimRef.current[9]=="." ? 
            [...prelimRef.current.substring(0,prelimRef.current.length-1).toLowerCase().split(" ")].slice(0,10) :
            [...prelimRef.current.toLowerCase().split(" ")].slice(0,10)
            );
            setTimeout(() => {
                setLoading(false);
            }, 1000);

    }
}

    return(
        <>
            <Topbar/>
            <div className="forum-wrapper">
                <Sidebar select={0} />
                <div className="forum-content-wrapper">
                    <div className="forum-content-subject">{id}</div>
                    <div className="forum-content-department-wrapper">
                        
                        <div className="forum-content-department-name"><Link to="/selection" style={{textDecoration:"none",color:"#FAF9F9"}}>CHANGE SUBJECT</Link></div>
                        
                        <div></div>
                    </div>
                    <div className="forum-content-heading">Forum</div>
                    <div className="forum-buttons-wrapper">
                        <div className="forum-buttons-nested-wrapper">
                            <svg role="button" onClick={()=>{setShowDisc(!showDisc); setShowSearch(false);}} width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="24" cy="24" r="24" fill="#EAC646"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M33.6749 16.9246C34.1082 17.3579 34.1082 18.0578 33.6749 18.491L31.6419 20.5241L27.4758 16.358L29.5088 14.3249C29.7164 14.1169 29.9982 14 30.2921 14C30.5859 14 30.8677 14.1169 31.0753 14.3249L33.6749 16.9246ZM14 33.4445V30.0672C14 29.9117 14.0555 29.7783 14.1666 29.6673L26.2872 17.5467L30.4533 21.7128L18.3216 33.8333C18.2216 33.9444 18.0772 34 17.9328 34H14.5555C14.2444 34 14 33.7556 14 33.4445Z" fill="#FAF9F9"/>
                            </svg>
                            <div className="forum-buttons-label">Start a discussion</div>
                        </div>
                        <div className="forum-buttons-nested-wrapper">
                            <svg role="button" onClick={()=>{setShowSearch(!showSearch);setShowDisc(false);}} width="48" height="48" viewBox="0 0 48 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="24" cy="24" r="24" fill="#EAC646"/>
                            <path fill-rule="evenodd" clip-rule="evenodd" d="M27.7283 26.8978H28.6545L33.6375 31.8925C34.1182 32.3732 34.1182 33.147 33.6375 33.6277L33.6257 33.6395C33.145 34.1202 32.3712 34.1202 31.8905 33.6395L26.8958 28.6565V27.7303L26.5792 27.402C24.9378 28.8089 22.6984 29.5359 20.3183 29.1372C17.0589 28.5862 14.456 25.866 14.0574 22.5832C13.4594 17.6236 17.6216 13.4497 22.5812 14.0594C25.864 14.458 28.5842 17.0609 29.1352 20.3203C29.5339 22.7004 28.8069 24.9398 27.4 26.5812L27.7283 26.8978ZM16.3438 21.6217C16.3438 24.5411 18.7004 26.8978 21.6199 26.8978C24.5393 26.8978 26.8959 24.5411 26.8959 21.6217C26.8959 18.7023 24.5393 16.3456 21.6199 16.3456C18.7004 16.3456 16.3438 18.7023 16.3438 21.6217Z" fill="#FAF9F9"/>
                            </svg>
                            <div className="forum-buttons-label">Browse the {id} forum</div>
    
                        </div>
                    </div>
                    <div style={{display:`${showDisc?"inherit":"none"}`}} className="forum-question-input-wrapper">
                        <div id="triangle-up"></div>
                        <div className="forum-question-input">
                        <input placeholder="Type your question here..." value={question} onChange={(e)=>setQuestion(e.target.value)} className="forum-question-input-question"/>
                        <textarea value={description} onChange={(e)=>setDescription(e.target.value)} className="forum-question-input-description" placeholder="Add a description of your question here...">
                        </textarea>
                        <div className="forum-question-input-buttons">
                            <div className="forum-question-input-image">
                                <input type="file" accept="image/*" onChange={e=>{previewFile(e.target.files[0]);setImage(e.target.files); setImageReset(e.target.value); }} ref={inputEl} className="forum-question-hide-input-file"/>
                                <svg role="button" onClick={()=>inputEl.current.click()} width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M21.6602 19V5C21.6602 3.9 20.7602 3 19.6602 3H5.66016C4.56016 3 3.66016 3.9 3.66016 5V19C3.66016 20.1 4.56016 21 5.66016 21H19.6602C20.7602 21 21.6602 20.1 21.6602 19ZM9.16016 13.5L11.6602 16.51L15.1602 12L19.6602 18H5.66016L9.16016 13.5Z" fill="black"/>
                                </svg>
                                <div>{name}</div>
                                {image  ?
                                <svg role="button" onClick={()=>{
                                    imagePreview.current.src = "";
                                    setImage(null);
                                    setImageReset("");
                                    setName(null);}} width="15" height="15" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M5 0C2.235 0 0 2.235 0 5C0 7.765 2.235 10 5 10C7.765 10 10 7.765 10 5C10 2.235 7.765 0 5 0ZM5 9C2.795 9 1 7.205 1 5C1 2.795 2.795 1 5 1C7.205 1 9 2.795 9 5C9 7.205 7.205 9 5 9ZM5 4.295L6.795 2.5L7.5 3.205L5.705 5L7.5 6.795L6.795 7.5L5 5.705L3.205 7.5L2.5 6.795L4.295 5L2.5 3.205L3.205 2.5L5 4.295Z" fill="#656D78"/>
                                </svg>:
                                ""
                                }
                            </div>
                            <div role={question?"button":""} onClick={()=>question?saveIt():""} className={question?"forum-question-input-post":"forum-question-input-post-disabled"}>
                                {loading?<CircularProgress style={{alignSelf:"center", marginRight:"8px",color:"#FFFFFF"}} size={20}/>:""}
                                <div>POST</div>
                            </div>
                        </div>   
                    </div>
                    </div>
                    
                    <div style={{display:`${showSearch?"inherit":"none"}`}} className="forum-question-search-wrapper">
                        <div id="search-triangle-up"></div>
                        <div className="forum-question-search">
                            <input placeholder={`Search ${id}...`} value={prelim} onKeyPress={e=>{if (e.key==="Enter") {searchIt();}}} onChange={(e)=>setPrelim(e.target.value)} className="forum-question-input-search"/>
                            <div onClick={()=>searchIt()} role="button" className="forum-question-search-button">
                                {loading?<CircularProgress style={{alignSelf:"center", marginRight:"8px",color:"#FFFFFF"}} size={20}/>:""}
                                <div>SEARCH</div>
                            </div>
                        </div>
                    </div>

                    <div ref={wrapperRef} className="sort-by-wrapper">
                        <div className="sort-by-label">Sort by</div>
                        <div role="button" onClick={()=>setShowSelect(!showSelect)} className="sort-by-display">
                            {select}
                            <svg width="14" height="8" viewBox="0 0 14 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.4958 0.355336L6.7821 5.06909L2.06834 0.355336C1.84136 0.127846 1.53321 0 1.21185 0C0.890489 0 0.582333 0.127846 0.355354 0.355336C-0.118451 0.829141 -0.118451 1.59452 0.355354 2.06832L5.93168 7.64465C6.40548 8.11845 7.17086 8.11845 7.64466 7.64465L13.221 2.06832C13.6948 1.59452 13.6948 0.829141 13.221 0.355336C12.7472 -0.10632 11.9697 -0.118469 11.4958 0.355336Z" fill="#EAC646"/>
                            </svg>
                        </div>
                        <div className="sort-by-choices-wrapper" style={{visibility:`${showSelect?"visible":"hidden"}`}}>
                            <div className={`sort-by-element ${select==="Most Likes"?"sort-by-element-selected":""}`} role="button" onClick={()=>{setOrderBy("likes"); setDesc("desc");setShowSelect(false);setSelect("Most Likes");}}>Most Likes</div>
                            <div className={`sort-by-element ${select==="Newest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc("desc");setShowSelect(false);setSelect("Newest");}}>Newest</div>
                            <div className={`sort-by-element ${select==="Oldest"?"sort-by-element-selected":""}`}  role="button" onClick={()=>{setOrderBy("timestamp"); setDesc("asc");setShowSelect(false);setSelect("Oldest");}}>Oldest</div>
                        </div>
                    </div>
                    <Questions orderby={orderBy} desc={desc} query={query}/>
                </div>
                <NotableQuestions/>
            </div>
            <FooterNew/>
        </>
    )
}


function Questions ({orderby,desc,query}){
    const array = Array.from(Array(5).keys())
    let { id } = useParams();

    const fire = useContext(FirebaseContext);
    const [name, setName]= useState([]);
    const [counter, setCounter]=useState(5);
    const click = useContext(ClickContext);
    const [menu, setMenu] = useState(false);
    const [user,setUser]=useState("");
    const wrapperRef = useRef(null);


    useEffect(() => {
        window.addEventListener("click", handleClickOutside);
        return () => {
          window.removeEventListener("click", handleClickOutside);
        };
      });
    
      const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
                setMenu(false);
          //console.log(event.target);
        }
      };        

    useEffect(()=>{
        if (fire.status){
            const afterQuery = fire.store.collection("forum").where("show","==",true).where("subjectCode","==",`${id}`).where("keywords","array-contains-any",query).orderBy(orderby,desc);
            const unsubscribe = afterQuery.limit(counter).onSnapshot(querySnapshot => {
                let z= [];
                querySnapshot.forEach(queryDocumentSnapshot => {
                    z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
                });
                setName(z);
            }
            )
            return unsubscribe
        }
    },[counter,orderby,desc,fire.store,fire.status,query])


    useEffect(()=>{
        if(fire.status){
        const docQuery = fire.store.collection("users").doc(fire.auth.currentUser.uid)
        docQuery.onSnapshot(querySnapshot => { 
            setUser(querySnapshot.data());
        }
        )
        }
    
    },[fire.status]);



    
    function notifyMe(e){
        fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uQuestions:fire.fieldValue.arrayUnion(e)
        })
    }

    function unNotifyMe(e){
        fire.store.collection("users").doc(fire.auth.currentUser.uid).update({
            uQuestions:fire.fieldValue.arrayRemove(e)
        })
    }

    async function userFlagIt(uid,docid){
        await fire.store.collection("forum").doc(docid).update({ 
            flaggedBy: fire.fieldValue.arrayUnion(uid),
        });
    }

    async function removeUserFlagIt(uid,docid){
        await fire.store.collection("forum").doc(docid).update({ 
            flaggedBy: fire.fieldValue.arrayRemove(uid),
        });
    }



    const questions = name.map((element,i)=>
        <div /* role="button" */    className="savedquestions-wrapper">
            <div className="savedquestions-subject">
                {id}
                <div key={i}className="savedquestions-subject-menu">   
                    <svg className="svg-hover" role="button" onClick={()=>{setMenu(i===0&& menu!==0?0:(menu-i===0)?false:i);}} width="32" height="8" viewBox="0 0 32 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="4" cy="4" r="4" fill="#828282"/>
                    <circle cx="16" cy="4" r="4" fill="#828282"/>
                    <circle cx="28" cy="4" r="4" fill="#828282"/>
                    </svg>
                    <div role="button" style={{display:`${menu===i?"flex":"none"}`}}  className="savedquestions-subject-menu-wrapper">
                        <div className="savedquestions-subject-menu-item"  onClick={()=>user.uQuestions.includes(element.id)?unNotifyMe(element.id):notifyMe(element.id)}>
                        <svg width="20" height="20" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16.0001 29.3333C17.4667 29.3333 18.6667 28.1333 18.6667 26.6666H13.3334C13.3334 28.1333 14.5201 29.3333 16.0001 29.3333ZM24.0001 21.3333V14.6666C24.0001 10.5733 21.8134 7.14665 18.0001 6.23998V5.33331C18.0001 4.22665 17.1067 3.33331 16.0001 3.33331C14.8934 3.33331 14.0001 4.22665 14.0001 5.33331V6.23998C10.1734 7.14665 8.00007 10.56 8.00007 14.6666V21.3333L6.28007 23.0533C5.44007 23.8933 6.02674 25.3333 7.2134 25.3333H24.7734C25.9601 25.3333 26.5601 23.8933 25.7201 23.0533L24.0001 21.3333Z" fill="#0A452D"/>
                        </svg>
                            <div style={{marginLeft:"8px"}}>{user.uQuestions?user.uQuestions.includes(element.id)?"Unnotify":"Notify":"Notify"}</div>
                        </div>
{/*                         <div className="savedquestions-subject-menu-item" onClick={()=>{}}>
                        <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M3 17.4601V20.5001C3 20.7801 3.22 21.0001 3.5 21.0001H6.54C6.67 21.0001 6.8 20.9501 6.89 20.8501L17.81 9.94006L14.06 6.19006L3.15 17.1001C3.05 17.2001 3 17.3201 3 17.4601ZM20.71 7.04006C21.1 6.65006 21.1 6.02006 20.71 5.63006L18.37 3.29006C17.98 2.90006 17.35 2.90006 16.96 3.29006L15.13 5.12006L18.88 8.87006L20.71 7.04006Z" fill="#0A452D"/>
                        </svg>
                            <div style={{marginLeft:"8px"}}>Edit</div>
                        </div> */}
                        <div className="savedquestions-subject-menu-item" onClick={()=>element.content.flaggedBy.includes(fire.auth.currentUser.uid)?removeUserFlagIt(fire.auth.currentUser.uid,element.id):userFlagIt(fire.auth.currentUser.uid,element.id)}>
                            <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.7667 2.5H7.23333C7.01667 2.5 6.8 2.59167 6.65 2.74167L2.74167 6.65C2.59167 6.8 2.5 7.01667 2.5 7.23333V12.7583C2.5 12.9833 2.59167 13.1917 2.74167 13.35L6.64167 17.25C6.8 17.4083 7.01667 17.5 7.23333 17.5H12.7583C12.9833 17.5 13.1917 17.4083 13.35 17.2583L17.25 13.3583C17.4083 13.2 17.4917 12.9917 17.4917 12.7667V7.23333C17.4917 7.00833 17.4 6.8 17.25 6.64167L13.35 2.74167C13.2 2.59167 12.9833 2.5 12.7667 2.5ZM10 14.4167C9.4 14.4167 8.91667 13.9333 8.91667 13.3333C8.91667 12.7333 9.4 12.25 10 12.25C10.6 12.25 11.0833 12.7333 11.0833 13.3333C11.0833 13.9333 10.6 14.4167 10 14.4167ZM10 10.8333C9.54167 10.8333 9.16667 10.4583 9.16667 10V6.66667C9.16667 6.20833 9.54167 5.83333 10 5.83333C10.4583 5.83333 10.8333 6.20833 10.8333 6.66667V10C10.8333 10.4583 10.4583 10.8333 10 10.8333Z" fill="#0A452D"/>
                            </svg>
                            <div style={{marginLeft:"8px"}}>{element.content.flaggedBy.includes(fire.auth.currentUser.uid)?"Unreport":"Report"}</div>
                        </div>
{/*                         <div className="savedquestions-subject-menu-item">
                        <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18 16.08C17.24 16.08 16.56 16.38 16.04 16.85L8.91 12.7C8.96 12.47 9 12.24 9 12C9 11.76 8.96 11.53 8.91 11.3L15.96 7.19C16.5 7.69 17.21 8 18 8C19.66 8 21 6.66 21 5C21 3.34 19.66 2 18 2C16.34 2 15 3.34 15 5C15 5.24 15.04 5.47 15.09 5.7L8.04 9.81C7.5 9.31 6.79 9 6 9C4.34 9 3 10.34 3 12C3 13.66 4.34 15 6 15C6.79 15 7.5 14.69 8.04 14.19L15.16 18.35C15.11 18.56 15.08 18.78 15.08 19C15.08 20.61 16.39 21.92 18 21.92C19.61 21.92 20.92 20.61 20.92 19C20.92 17.39 19.61 16.08 18 16.08Z" fill="#0A452D"/>
                        </svg>
                            <div style={{marginLeft:"8px"}}>Share</div>
                        </div> */}
                    </div>
                </div>
            </div>
            <div className="savedquestions-question">
                <Link to={`/forum/${id}/${element.id}`} style={{textDecoration:"none",color:"inherit"}}>
                    <Latex>{element.content.question}</Latex>
                </Link>
                {user.uQuestions?user.uQuestions.includes(element.id)?<svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M16.0001 29.3333C17.4667 29.3333 18.6667 28.1333 18.6667 26.6666H13.3334C13.3334 28.1333 14.5201 29.3333 16.0001 29.3333ZM24.0001 21.3333V14.6666C24.0001 10.5733 21.8134 7.14665 18.0001 6.23998V5.33331C18.0001 4.22665 17.1067 3.33331 16.0001 3.33331C14.8934 3.33331 14.0001 4.22665 14.0001 5.33331V6.23998C10.1734 7.14665 8.00007 10.56 8.00007 14.6666V21.3333L6.28007 23.0533C5.44007 23.8933 6.02674 25.3333 7.2134 25.3333H24.7734C25.9601 25.3333 26.5601 23.8933 25.7201 23.0533L24.0001 21.3333Z" fill="#EBC74E"/>
                    </svg>
                    :"":""}
            </div>
            {/* <div className="savedquestions-description">{element.content.description}</div> */}
            <div className="savedquestions-menu-wrapper">
                <div className="savedquestions-menu-like">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M1.74561 21H5.74561V9H1.74561V21ZM23.7456 10C23.7456 8.9 22.8456 8 21.7456 8H15.4356L16.3856 3.43L16.4156 3.11C16.4156 2.7 16.2456 2.32 15.9756 2.05L14.9156 1L8.33561 7.59C7.96561 7.95 7.74561 8.45 7.74561 9V19C7.74561 20.1 8.64561 21 9.74561 21H18.7456C19.5756 21 20.2856 20.5 20.5856 19.78L23.6056 12.73C23.6956 12.5 23.7456 12.26 23.7456 12V10Z" 
                        fill={(element.content.likers?element.content.likers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                        <div className="savedquestions-menu-like-number">{element.content.likes?element.content.likes:"0"}</div>
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M15.7456 3H6.74561C5.91561 3 5.20561 3.5 4.90561 4.22L1.88561 11.27C1.79561 11.5 1.74561 11.74 1.74561 12V14C1.74561 15.1 2.64561 16 3.74561 16H10.0556L9.10561 20.57L9.07561 20.89C9.07561 21.3 9.24561 21.68 9.51561 21.95L10.5756 23L17.1656 16.41C17.5256 16.05 17.7456 15.55 17.7456 15V5C17.7456 3.9 16.8456 3 15.7456 3ZM19.7456 3V15H23.7456V3H19.7456Z" 
                        fill={(element.content.dislikers?element.content.dislikers.includes(fire.auth.currentUser.uid):false)?"#EAC646":"#E0E0E0"}/>
                    </svg>
                </div>
                <div className="savedquestions-menu-timestamp">{element.content.timestamp?(new Date(element.content.timestamp.seconds*1000)).toLocaleString():"Loading Timestamp"} </div>
                <div className="savedquestions-menu-comments">
                    <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M20.1685 2H4.16846C3.06846 2 2.16846 2.9 2.16846 4V16C2.16846 17.1 3.06846 18 4.16846 18H18.1685L22.1685 22V4C22.1685 2.9 21.2685 2 20.1685 2ZM18.1685 14H6.16846V12H18.1685V14ZM18.1685 11H6.16846V9H18.1685V11ZM18.1685 8H6.16846V6H18.1685V8Z" fill="black"/>
                    </svg>
                    <div className="savedquestions-menu-comments-number">{element.content.answersLength}</div>
                </div>
            </div>
            
        </div>

    )

        return(
            <>
            <div className="dashboard-content-savedquestions"ref={wrapperRef}>
                {questions}
                <div className="dashboard-content-savedquestions-viewall-wrapper">
                    <div role="button" onClick={()=>setCounter(y=>y+5)} style={{display:`${name.length>5&&name.length%5||name.length==0||name.length<5?"none":"flex"}`}} className="dashboard-content-savedquestions-viewall">See More</div>
                </div>
                <div style={{display:`${name.length==0?"flex":"none"}`}} className="dashboard-content-body">
        <div>
            <svg width="280" height="404" viewBox="0 0 280 404" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M17.8511 184.841H142.999C143.433 184.841 143.85 185.014 144.158 185.322C144.465 185.629 144.638 186.046 144.638 186.481V196.125C144.638 196.56 144.465 196.977 144.158 197.285C143.85 197.592 143.433 197.765 142.999 197.765H77.8872V220.999" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M32.619 11.2877L26.3909 56.908C24.8263 68.3256 26.5897 79.9522 31.4694 90.3923L40.573 109.794C46.6217 122.686 57.0638 132.756 69.8498 138.03L97.3366 149.369L32.619 11.2877Z" fill="white" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M53.3622 184.886H16.0321C7.54976 184.886 1.33129 176.561 3.39984 167.963L20.6912 109.451C22.2922 102.784 28.0368 98.1125 34.6203 98.1125" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M99.7093 151.943C99.3545 151.94 99.008 151.836 98.7106 151.642C98.4133 151.449 98.1776 151.174 98.0314 150.851L32.1322 10.1893C31.5238 8.89249 32.43 7.3811 33.8101 7.3811C34.1669 7.38218 34.5158 7.48564 34.8155 7.6792C35.1152 7.87276 35.3531 8.14827 35.5008 8.47302L101.387 149.135C101.996 150.432 101.089 151.943 99.7093 151.943Z" fill="white" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M58.5755 184.063C61.2371 184.063 63.3947 181.906 63.3947 179.244C63.3947 176.583 61.2371 174.425 58.5755 174.425C55.914 174.425 53.7563 176.583 53.7563 179.244C53.7563 181.906 55.914 184.063 58.5755 184.063Z" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M53.7718 179.891C53.6854 176.951 55.6226 174.511 58.575 174.425L59.0425 174.409C60.2955 174.373 61.548 174.496 62.7697 174.777L101.762 183.801" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M277.515 136.281L246.561 250.34C244.991 256.131 241.56 261.244 236.795 264.891C232.03 268.537 226.197 270.513 220.198 270.513H115.348" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10"/>
        <path d="M123.824 270.501L208.34 398.719" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M216.346 270.501L124.145 400" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M125.214 354.962C125.214 354.962 120.818 361.687 113.184 360.745L108.022 353.358C113.347 352.213 118.557 350.591 123.591 348.51C124.116 350.966 124.663 353.217 125.214 354.962Z" fill="#FFCFC9"/>
        <path d="M234.031 237.676C230.938 244.602 220.633 262.598 196.931 263.344C167.549 264.269 113.491 261.496 113.491 261.496C113.491 261.496 125.854 310.348 126.12 347.367C126.12 347.367 116.376 352.311 101.912 354.482C101.912 354.482 50.0383 257.459 60.3395 233.737C68.0053 216.091 126.271 209.229 152.608 207.013C163.553 213.359 199.97 233.247 234.031 237.676Z" fill="#144A64"/>
        <path d="M125.214 354.962C125.214 354.962 130.555 360.835 136.45 365.497C140.183 368.45 138.717 374.143 135.537 378.927C130.756 386.101 125.449 392.911 119.661 399.302C119.661 399.302 111.39 400.176 95.2933 399.516C95.2933 399.516 94.6913 393.804 100.381 391.223C113.35 385.341 116.27 369.769 113.19 360.761C113.183 360.745 119.661 361.901 125.214 354.962Z" fill="#144A64"/>
        <path d="M138.989 367.166C140.539 370.454 138.233 375.398 135.858 378.927C135.858 378.927 127.026 391.239 120.302 399.606L94.9731 399.821" stroke="#D8BAA3" stroke-width="7" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M111.544 386.919C111.544 386.919 105.7 383.096 109.943 380.759" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M114.746 383.397C114.746 383.397 107.621 379.574 111.864 377.236" stroke="#D8BAA3" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M201.171 76.4376C201.171 76.4376 228.004 69.044 229.903 47.0872C230.227 43.3695 201.171 76.4376 201.171 76.4376Z" fill="#D6B03F"/>
        <path d="M201.494 33.5231C201.494 33.5231 213.076 57.7405 223.733 66.7801C223.733 66.7801 206.72 78.0931 192.439 78.0867C192.439 78.0867 183.088 61.6215 179.214 55.323L201.494 33.5231Z" fill="#FFCFC9"/>
        <path d="M187.972 70.2704C185.016 65.1246 181.311 58.7428 179.214 55.323L196.166 38.7457L197.328 41.0608C198.564 52.1593 195.468 64.2696 187.972 70.2704Z" fill="#FFC0B7"/>
        <path d="M197.776 38.3646C197.776 38.3646 197.725 47.6507 194.193 52.7901C189.749 59.2615 178.436 66.2389 169.825 64.9581C161.215 63.6772 153.706 20.0679 160.164 17.487C166.623 14.9061 184.667 12.5365 190.046 20.4937C195.426 28.4509 197.776 38.3646 197.776 38.3646Z" fill="#FFCFC9"/>
        <path d="M206.813 43.7474C206.813 43.7474 203.063 44.3493 197.613 41.8485C197.783 39.812 197.86 35.63 195.753 34.426C193.454 33.1132 192.141 36.818 191.734 38.2205C191.243 37.8405 190.747 37.4317 190.245 36.9941C181.28 29.2034 183.614 21.576 183.614 21.576C183.614 21.576 170.274 29.4212 157.565 28.9024C147.017 28.4733 141.852 13.7117 150.744 9.31199C150.744 9.31199 150.104 13.3562 157.866 11.419C165.628 9.4817 164.119 1.60134 174.997 0.214832C188.401 -1.47908 191.171 7.40034 191.171 7.40034C191.171 7.40034 197.591 4.02213 204.2 10.167C211.242 16.7152 202.144 34.9928 206.813 43.7474Z" fill="#383C4D"/>
        <path d="M184.984 20.1543C184.984 20.1543 172.515 30.1001 158.964 31.8644C146.216 33.5231 140.632 17.0611 149.294 11.0924" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
        <path d="M190.018 8.0631C190.018 8.0631 194.376 1.86384 202.627 5.50142" stroke="#383C4D" stroke-width="4" stroke-miterlimit="10"/>
        <path d="M236.849 238.009C246.839 228.224 259.565 166.51 256.241 126.944C256.033 122.44 255.373 117.968 254.272 113.595C253.705 111.02 252.946 108.492 252.001 106.031C249.119 98.6667 242.529 90.9816 233.365 85.7334C224.025 79.0442 211.837 74.4172 195.686 77.1197C171.645 81.1416 166.063 105.141 164.77 116.067L164.75 116.048L158.228 152.718L110.632 154.514V170.083C110.632 170.083 149.688 193.907 177.354 187.003L177.597 186.939C164.469 199.779 149.316 205.063 149.316 205.063C149.316 205.063 195.417 233.664 236.849 238.009Z" fill="#EAC646"/>
        <path d="M200.729 136.845C200.729 136.845 191.616 182.747 174.674 188.604L200.729 136.845Z" fill="#EAC646"/>
        <path d="M200.729 136.845C200.729 136.845 191.616 182.747 174.674 188.604" stroke="white" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M201.171 76.4377C208.597 75.477 228.273 62.9664 229.887 46.498C229.887 46.498 259.471 76.4377 248.636 100.361C248.636 100.361 242.603 89.1532 229.558 82.3423C213.944 74.1994 201.171 76.4377 201.171 76.4377Z" fill="#EAC646"/>
        <path d="M251.252 101.657C251.252 101.657 242.286 89.7392 229.558 82.3423C211.738 71.9836 189.816 77.4111 189.816 77.4111" fill="#EAC646"/>
        <path d="M251.252 101.657C251.252 101.657 242.286 89.7392 229.558 82.3423C211.738 71.9836 189.816 77.4111 189.816 77.4111" stroke="white" stroke-width="5" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M110.785 164.55C110.785 164.55 106.056 172.027 101.022 170.954C95.9882 169.881 91.3035 169.145 88.7514 170.458C85.5974 172.081 82.4657 175.664 80.4068 175.597C77.9142 175.532 75.4574 174.988 73.17 173.996C74.4656 171.355 75.8934 168.782 77.448 166.285C75.7029 167.39 73.3878 169.657 72.1069 170.515C70.0576 171.892 68.623 169.932 68.623 169.932C68.623 169.932 72.1646 164.902 73.7144 163.592C75.2034 162.334 80.5765 158.431 82.9204 158.085C85.7351 157.415 99.6226 155.382 110.817 154.617L110.785 164.55Z" fill="#FFCFC9"/>
        <path d="M75.7827 174.066C75.7827 174.066 82.062 164.87 83.8072 164.223C86.8812 163.086 90.8902 161.585 90.8902 161.585" stroke="#F7B3AF" stroke-width="4" stroke-miterlimit="10" stroke-linecap="round"/>
        <path d="M78.8408 174.899C78.8408 174.899 84.5149 167.214 86.2569 166.573C89.3309 165.437 93.3431 163.932 93.3431 163.932" stroke="#F7B3AF" stroke-width="4" stroke-miterlimit="10" stroke-linecap="round"/>
        </svg>
        </div>
        <div className="selection-warning">
        No questions yet.<br/>Ask the first one!
        </div>
    </div>
            </div>
            </>
        )
}

export function Sidebar({subject,select}){
    let { id } = useParams();
    const {location} = useLocation();

    return(
        <div className="sidebar-wrapper">
            <div className="sidebar-selected-wrapper">
                {select===0?<svg className="sidebar-selector-margin" width="8" height="24" viewBox="0 0 8 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <rect width="8" height="24" fill="#EBC74E"/>
                </svg>:""}
                <div className={select===0?"sidebar-selected":"sidebar-element"}>
                    <Link to={`/forum/${sessionStorage.getItem("subjectcode")?sessionStorage.getItem("subjectcode"):"MATH10"}`}  style={{textDecoration:"none",color:"#000000"}}>
                        Forum
                    </Link>
                </div>
            </div>
            <div className={select===1?"sidebar-selected-wrapper":""}>
                {select===1?<svg className="sidebar-selector-margin-exam" width="8" height="24" viewBox="0 0 8 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <rect width="8" height="24" fill="#EBC74E"/>
                    </svg>:""}
                <div className={select===1?"sidebar-selected-exam":"sidebar-element"}>
                    <Link to={`/exams`}  style={{textDecoration:"none",color:"#000000"}}>
                    Long Exams
                    </Link>
                </div>
            </div>
            <div className={select===2?"sidebar-selected-wrapper":""}>
                <div key={2} className="sidebar-element-disabled">
                    Tutors<br/>(Coming Soon!)
                </div>
            </div>
        </div>
    )
}

export function NotableQuestions(){

    const fire = useContext(FirebaseContext);
    const [name, setName]=useState([]);
    const [nameTwo, setNameTwo]=useState([]);

    useEffect(()=>{
    if (fire.status){
        const afterQuery = fire.store.collection("forum").where("show","==",true).orderBy("likes","desc");
        const unsubscribe = afterQuery.limit(3).onSnapshot(querySnapshot => {
            let z= [];
            querySnapshot.forEach(queryDocumentSnapshot => {
                z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
            });
            setName(z);
        }
        )
        return unsubscribe
    }
},[fire.store,fire.status])


useEffect(()=>{
    if (fire.status){
        const afterQuery = fire.store.collection("forum").where("show","==",true).where("answersLength","==",0).orderBy("timestamp","desc");
        const unsubscribe = afterQuery.limit(3).onSnapshot(querySnapshot => {
            let z= [];
            querySnapshot.forEach(queryDocumentSnapshot => {
                z.push({id:queryDocumentSnapshot.id, content:queryDocumentSnapshot.data()});   
            });
            setNameTwo(z);
        }
        )
        return unsubscribe
    }
},[fire.store,fire.status])



    return(
        <div className="notable-questions-wrapper">
            <div className="notable-questions-content">
            <div className="notable-questions-content-header">Top Questions</div>
                {name.map((element, i)=>
                <div className={`${i<4?`bcolor${i+1}`:`bcolor${((i+1)%4)?((i+1)%4):4}`}`}>
                <Link to={`/forum/${name.length?element.content.subjectCode:""}/${name.length?element.id:""}`} style={{color:"inherit"}}>

                    <div className="notable-questions-content-subheader">
                        {name.length?element.content.subjectCode:""}
                    </div>
                    <div className="notable-questions-content-body">
                            <Latex>{name.length?`${element.content.question}`:""}</Latex>
                    </div>
                    </Link>
                </div>
                    )}      
            </div>
            <div className="notable-questions-content">
                <div className="notable-questions-content-header">Unanswered Questions</div>
                {nameTwo.map((element, i)=>
                <div className={`${i<4?`bcolor${i+1}`:`bcolor${((i+1)%4)?((i+1)%4):4}`}`}>
            <Link to={`/forum/${nameTwo.length?element.content.subjectCode:""}/${nameTwo.length?element.id:""}`} style={{color:"inherit"}}>

                    <div className="notable-questions-content-subheader">
                        {nameTwo.length?element.content.subjectCode:""}
                    </div>
                    <div className="notable-questions-content-body">
                            <Latex>{nameTwo.length?`${element.content.question}`:""}</Latex>
                    </div>
                    </Link>
                </div>
                    )}  
            </div>
        </div>  
    )
}