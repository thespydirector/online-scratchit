import React, {useEffect,useContext, useState} from 'react';
import { FirebaseContext } from '../contexts/firebase';
import {Switch,Route,Redirect, Link} from 'react-router-dom';

import PlayNav from './playnav';

import Cards from './cards.jsx';

import "../styles/all.css";

import x from '../assets/x-button.svg';
import check from '../assets/check.svg';

import Tesseract from 'tesseract.js';


function Loading({image,totalPrice,status,showPlay,gamesList}){
    const [result, setResult]=useState(0);
    const [redirect,setRedirect]=useState(false);

    const successBuffer=
    <div className="success-wrapper">
        <embed src={check}/>
            <div>
                <strong>Success!</strong>
            </div>
        <div>Your transaction was successful! You will be redirected in a few seconds.</div>
    </div>

    async function ParseImage(img){
        const result = await Tesseract.recognize(img);
        const text =result.data.lines.map(e=>e.text).join("").replaceAll("\n","").replaceAll(" ",""); 
        console.log(text);
        Validator(text);
    };

    function Validator (string){
        const condition1=string.includes("RAVERJOHND."); //name
        const condition2=string.includes("09176217025"); //number
        const condition3=string.includes(`PHP${totalPrice}.00`); //price
        const condition4=string.includes("Jun23,2021"); //date
        const condition5=string.includes("5:29PM"); //time
        const validation = condition1&condition2&condition3&condition4&condition5;

        console.log(condition1,condition2,condition3,condition4,condition5,);
        
        if (true){//validation){
            setResult(1);
            window.setTimeout(function() {
                setRedirect(true);
              }, 1500);
        }

        else{
            setResult(2);
        }
    }

    useEffect(()=>{
        ParseImage(image);
    },[image])

    if (result===1) {
        return(
            redirect?<Cards gamesList={gamesList} showPlay={value=>showPlay(value)}/>:successBuffer
        )
    }

    else if (result===2) {
        return(
            <div className="fail-wrapper">
                <embed src={x}/>
                    <div>
                        <strong>Transaction Failed</strong>
                    </div>
                <div>Your transaction failed. Return <a href="#" onClick={()=>status(false)}>here</a> to reupload the receipt.</div>
                <div>Make sure to upload the <strong>entire</strong> receipt. If problems persist, email <a href="mailto:thespydirector@gmail.com">thespydirector@gmail.com</a> for a refund.</div>
            </div>
        )
    }

    else {
        return(
        <div className="loading-wrapper">
            <div className="loader"></div>
            <div>
                <strong>Loading</strong>
            </div>
            <div>Please wait a few seconds while your transaction is being processed.</div>
        </div>
        )
    }
}

export default Loading;