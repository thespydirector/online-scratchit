import React, {useEffect,useContext, useState, useRef} from 'react';
import {Switch,Route,Redirect, Link} from 'react-router-dom';

import "../styles/all.css";

import redhot from '../assets/redhot.jpg';
import redhot2 from '../assets/redhot2.jpg';
import bananas from '../assets/bananas.jpg';
import bananas2 from '../assets/bananas2.jpg';
import lucky from '../assets/lucky2.jpg';
import lucky2 from '../assets/lucky2.jpg';
import moneytree from '../assets/moneytree.jpg';
import moneytree2 from '../assets/moneytree2.jpg';
import golds from '../assets/golds.jpg';
import golds2 from '../assets/golds2.jpg';
import cash from '../assets/cash.jpg';
import cash2 from '../assets/cash2.jpg';
import goldm from '../assets/goldm.jpg';
import goldm2 from '../assets/goldm2.jpg';
import goldl from '../assets/goldl.jpg';
import goldl2 from '../assets/goldl2.jpg';

import gcash from '../assets/gcash.png';

const cards = [
    {"name":"Go Bananas!","src":[bananas,bananas2],"position":"bananas-position","bonus":true,"bonus-position":"bananas-bonus","size":"s"},
    {"name":"Red Hot 7s","src":[redhot,redhot2],"position":"redhot-position","bonus":false,"bonus-position":"redhot-bonus","size":"s"},
    {"name":"Money Tree","src":[moneytree,moneytree2],"position":"moneytree-position","bonus":false,"bonus-position":"moneytree-bonus","size":"s"},
    {"name":"Lucky Sheep","src":[lucky,lucky2],"position":"lucky-position","bonus":true,"bonus-position":"lucky-bonus","size":"s"},
    {"name":"Go for Gold! (S)","src":[golds,golds2],"position":"golds-position","bonus":true,"bonus-position":"golds-bonus","size":"s"},
    {"name":"Cash Extravaganza","src":[cash,cash2],"position":"cash-position","bonus":true,"bonus-position":"cash-bonus","size":"m"},
    {"name":"Go for Gold! (M)","src":[goldm,goldm2],"position":"goldm-position","bonus":true,"bonus-position":"goldm-bonus","size":"m"},
    {"name":"Go for Gold! (L)","src":[goldl,goldl2],"position":"goldl-position","bonus":true,"bonus-position":"goldl-bonus","size":"l"},
]






function Cards({showPlay,gamesList}){
    const [display,setDisplay]=useState(false);
    const [summary,setSummary]=useState(false);
    const [num,setNum]=useState([0]);

    const [counter,setCounter]=useState(0);

    const [output,setOutput]=useState();

    useEffect(()=>{
        const output= ()=>
        <tbody>
            <tr>
                <td>{Math.floor(Math.random()*(10))}</td>
                <td>{Math.floor(Math.random()*(10))}</td>
                <td>{Math.floor(Math.random()*(10))}</td>
            </tr>
            <tr>
                <td>{Math.floor(Math.random()*(10))}</td>
                <td>{Math.floor(Math.random()*(10))}</td>
                <td>{Math.floor(Math.random()*(10))}</td>
            </tr>
            <tr>
                <td className="winning-num">7</td>
                <td>7</td>
                <td>7</td>
            </tr>
        </tbody>

        setOutput(output);
    },[counter])

    const result={
        table(size){
            if (size==="s"){
                return(
                    <table>
                        {output}
                </table>
                )
            }
    
            else if (size==="m"){
                return(
                    <div className="scratch-card-table-m-wrapper">
                    <table className="scratch-card-table-m-winning">
                        <tbody>
                            <tr>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>1</td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>2</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td className="winning-num">7</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td className="winning-num">7</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                )
            }
            else if (size==="l"){
                return(
                    <div className="scratch-card-table-l-wrapper">
                    <table className="scratch-card-table-l-winning">
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                                <td>1</td>
                            </tr>
                        </tbody>
                    </table>
                    <table>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td className="winning-num">7</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td className="winning-num">7</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>2</td>
                                <td>3</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td className="winning-num">7</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                            <tr>
                                <td className="winning-num">7</td>
                                <td>7</td>
                                <td>7</td>
                                <td>2</td>
                                <td>3</td>
                                <td>3</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                )
            }
        }
    }

    //retain played games
    //const games = gamesList.filter(e=>e.qty>0).map(e=>e.name);
    //cards = cards.filter(e=>games.includes(e.name));

    //goal, array of card names mapped to card num depending on order
    //array of objs (name,img,color,qty) with qty >0
    const filteredGames = gamesList.map(e=>e.qty);
    const numSequence = [];



    //


    const [loading,setLoading]=useState(true);

    //Scratch-off Code
    const [imageNew,setImageNew]=useState(null);
    const imageRef = useRef();
    imageRef.current=imageNew;

    const numRef = useRef(0);
    numRef.current=num;

    //const canvas = document.getElementById('canvas');

    const canvas = document.getElementById(`canvas${counter}`);
    const ctx = canvas?canvas.getContext('2d'):'';

    //const image = document.getElementById(`SourceImage`);

    const image = document.getElementById(`SourceImage${counter}`);

    //const [image,setImage] = useState(new Image(document.getElementById('SourceImage')));


    useEffect(()=>{
        //const canvas = document.getElementById(`canvas${counter}`);
        //const ctx = canvas?canvas.getContext('2d'):'';
        //const image = document.getElementById(`SourceImage${counter}`);
        //console.log(games,cardsNew);

        //function definitions
        function pick(event) {
            var x = event.offsetX;
            var y = event.offsetY;
            const coord = {"x":x,"y":y}

            return coord;
        }

        function touchpick(event,rect) {
            var x = Math.ceil(event.targetTouches[0].pageX - rect.left);
            var y = Math.ceil(event.targetTouches[0].pageY - rect.top);
            const coord = {"x":x,"y":y};
            //console.log("coords",coord);

            return coord;
        }

        function transparent(x,y) {
            //console.log("xy",x,y);
            //ctx.drawImage(image, 0, 0,image.width,image.height);
            const imageData = ctx.getImageData(0, 0, image.width, image.height);
            //const data = imageNew?imageNew.data:imageData.data;
            //const data = imageData.data;
            const data = imageRef.current?imageRef.current.data:imageData.data;
            //console.log("data",data);

            //radius of transparency
            for (let yNew = y-25; yNew <= y+25; yNew += 1) {
                //console.log("coords",yNew);
                for (let i = (4*(yNew*350+x))+100; i >= (4*(yNew*350+x))-100; i -= 4) { //orig: canvas.width
                    data[i+3]=0;
                }
            }

/*             for (let yNew = y+25; yNew >= y; yNew -= 1) {
                for (let i = 4*(yNew*image.width+x)-100; i <= 4*(yNew*image.width+x)+100; i += 4) {
                    data[i+3]=0; 
                }
            } */
            
            //ctx.putImageData(imageData, 0, 0);

            ctx.putImageData(imageRef.current?imageRef.current:imageData, 0, 0);
            imageRef.current?setImageNew(imageRef.current):setImageNew(imageData);

            //ctx.putImageData(imageNew?imageNew:imageData, 0, 0);
            //imageNew?setImageNew(imageNew):setImageNew(imageData);  
            //console.log("image new",imageRef.current);

        };

        //paintbrush behavior
        function mouseMoveWhilstDown(target, whileMove) {
            const endMove = function () {
                window.removeEventListener('mousemove', whileMove);
                window.removeEventListener('mouseup', endMove);
            };
        
            target.addEventListener('mousedown', function (event) {
                //event.stopPropagation(); // remove if you do want it to propagate ..
                window.addEventListener('mousemove', whileMove);
                window.addEventListener('mouseup', endMove);   
            });

        }

        function touchMoveWhilstDown(target, whileMove) {
            const endMove = function () {
                window.removeEventListener('touchmove', whileMove);
                window.removeEventListener('touchend', endMove);
            };

            target.addEventListener('touchstart', function (event) {
                //event.stopPropagation(); // remove if you do want it to propagate .
                window.addEventListener('touchmove', whileMove,{passive:true});
                window.addEventListener('touchend', endMove,{passive:true});   
            },{passive:true});
        }

        //sequence of nums to show
        for (let i=0; i<filteredGames.length; i++){
            for (let n=0; n<filteredGames[i]; n++){
                numSequence.push(i);
            }
        }

        setNum(numSequence);
        console.log("num",num);
        console.log("num2",counter,ctx,image,counter%2);

        //operation
        if(image&&canvas){
            //image.onload=()=>{
                //ctx.clearRect(0, 0, 350, 600);
                //setLoading(false);
/*                 image.onload=()=>{
                    setTimeout(setLoading(false),1000);
                    ctx.drawImage(image,0,0,image.width,image.height);
                }; */

                console.log("whey",image.width,image.height);

                image.onload=()=>{
                    canvas.width =  image.width; 
                    canvas.height = image.height;
                    ctx.drawImage(image,0,0,image.width,image.height);
                }
                //image.src=cards[numRef.current].src;
                //image.src=cards[numSequence[0]].src;

                //setImage(y=>y.src=cards[numRef.current].src);
                //ctx.drawImage(image,0,0,image.width,image.height);
                //setImage(y=>y.onload=()=>ctx.drawImage(y,0,0,y.width,y.height))
                 //apparently you have to wait for the image to load


                //image.onload=()=>{
                    //ctx.drawImage(image,0,0,image.width,image.height);
                //canvas.width = 350;
                //canvas.height = 600;
                //canvas.height = cards[numRef.current].size==="l"?625:cards[numRef.current].size==="m"?400:192;

                mouseMoveWhilstDown(canvas,(event)=>{ 
                    transparent(pick(event).x,pick(event).y);

                });

                touchMoveWhilstDown(canvas,(event)=>{ 
                    const rect = event.target.getBoundingClientRect();
                    transparent(touchpick(event,rect).x,touchpick(event,rect).y);
                });
            //}
            //}
        }
    //},[])
    },[canvas,ctx,image])


if(summary===true){
    return(
        <Payout showPlay={value=>showPlay(value)}/>
    )
}
// /{setNum(y=>y-1);setImageNew(null);}

/* else if (loading===true){
    return(
        <div className="loading-wrapper">
            <div className="loader"></div>
            <div>
                <strong>Loading your scratch cards</strong>
            </div>
            <div></div>
        </div>
    )
} */

else{
    return(
        <div className="scratch-card-wrapper no-scroll">
            <div className="scratch-card-menu-wrapper">
                {/* <div className={`${counter>0?'':'hide'}`} onClick={()=>{setCounter(y=>y-1);setImageNew(null);}}>Previous</div> */}
                <div onClick={()=>setSummary(true)}>Go to Payout Summary</div>
                <div className={`${counter<num.length-1?'':'hide'}`} onClick={()=>{setCounter(y=>y+1);setImageNew(null);}}>Next</div>
            </div>
            <header>
                Tap the card to get the result!
            </header>
             <div className="scratch-card">
                <div className={`scratch-bonus-wrapper ${cards[num[counter]].bonus?`show-bonus ${cards[num[counter]]["bonus-position"]}`:'hide-bonus'} move-to-back`}>
                    <div>BONUS</div><div>x2</div>
                </div>
                <div className={`scratch-bonus-wrapper ${cards[num[counter]].size==="l"?`show-bonus ${cards[num[counter]]["bonus-position"]}2`:'hide-bonus'} move-to-back`}>
                    <div>BONUS</div><div>x2</div>
                </div>
                <div className={`scratch-bonus-wrapper ${cards[num[counter]].size==="l"?`show-bonus ${cards[num[counter]]["bonus-position"]}3`:'hide-bonus'} move-to-back`}>
                    <div>BONUS</div><div>x2</div>
                </div>
{/*                 {loading?        <div className="loading-wrapper">
            <div className="loader"></div>
            <div>
                <strong>Loading your scratch cards</strong>
            </div>
            <img className="scratch-card-img hide-scratch-img" id="SourceImage" src={cards[num].src}/>
                    <canvas className="scratch-card-img" id="canvas"></canvas>
        </div>:<><img className="scratch-card-img hide-scratch-img" id="SourceImage" src={cards[num].src}/>
                    <canvas className="scratch-card-img" id="canvas"></canvas></>} 

                    <img className="scratch-card-img hide-scratch-img" id={`SourceImage`} key={counter} src={cards[num[counter]].src}/>
                    <canvas className="scratch-card-img" id="canvas"></canvas>

                    <img className="scratch-card-img hide-scratch-img" id={`SourceImage${counter}`} src={cards[num[counter]].src}/>

                    <img className="scratch-card-img hide-scratch-img" id={`SourceImage`} key={counter} src={cards[num[counter]].src}/>
                    <canvas className="scratch-card-img" id={`canvas${counter}`}></canvas>
                <div className={`scratch-content ${cards[num[counter]].position} move-to-back`}>
                    {result.table(cards[num[counter]].size)}
                </div>

                    */}
                    
                    <Card num={num} counter={counter} result={result}/>
            </div>
            <div>
                
            </div>
        </div>
    )
}
}


function Card({num,counter,result}){
    return(
        <>
            <img className="scratch-card-img hide-scratch-img" id={`SourceImage${counter}`} src={cards[num[counter]].src[counter%2]}/>
            <canvas className="scratch-card-img" id={`canvas${counter}`}></canvas>
            <div className={`scratch-content ${cards[num[counter]].position} move-to-back`}>
                {result.table(cards[num[counter]].size)}
            </div>
        </>
    )
}


function Payout({showPlay}){
    return(
        <article className="payout-summary-wrapper">
            <canvas className="scratch-card-img hide" id="canvas"></canvas>
            <img className="scratch-card-img hide" id="SourceImage" src={bananas}/>
        <header>
            Payout Summary
        </header>
        <section>
            <table>
                <thead>
                    <tr>
                        <th>Card</th>
                        <th>Qty</th>
                        <th>Winnings</th>
                    </tr>
                </thead>
                <tbody>
                    <tr className="order-row">
                        <td>Go Bananas!</td>
                        <td>4</td>
                        <td>₱80.00</td>
                    </tr>
                    <tr>
                        <td>Red Hot 7s</td>
                        <td>6</td>
                        <td>₱120.00</td>
                    </tr>
                    <tr className="final-row">
                        <td>Grand Winnings</td>
                        <td></td>
                        <td>₱200.00</td>
                    </tr>
                </tbody>
            </table>
        </section>
        <div className="scratch-card-menu-wrapper">
            <div>
                <a href="#" className="no-text-decorations" onClick={()=>window.location.reload()}>
                Buy Again
                </a>
            </div>
        </div>
        </article>
    )
}

export default Cards;


/**>window.location.reload()
 *     return(
        <div className="scratch-card-wrapper">
            <div className="scratch-card-menu-wrapper">
                <div className={`${num>0?'':'hide-card'}`} onClick={()=>setNum(y=>y-1)}>Previous</div>
                <div onClick={()=>setSummary(true)}>Scratch All</div>
                <div className={`${num<7?'':'hide-card'}`} onClick={()=>setNum(y=>y+1)}>Next</div>
            </div>
            <header>
                Tap the card to get the result!
            </header>
             <div className="scratch-card">
                <div className={`scratch-bonus-wrapper ${display&&cards[num].bonus?`show-bonus ${cards[num]["bonus-position"]}`:'hide-bonus'}`}>
                    <div>BONUS</div><div>x2</div>
                </div>
                <div className={`scratch-bonus-wrapper ${display&&cards[num].size==="l"?`show-bonus ${cards[num]["bonus-position"]}2`:'hide-bonus'}`}>
                    <div>BONUS</div><div>x2</div>
                </div>
                <div className={`scratch-bonus-wrapper ${display&&cards[num].size==="l"?`show-bonus ${cards[num]["bonus-position"]}3`:'hide-bonus'}`}>
                    <div>BONUS</div><div>x2</div>
                </div>
                <img onClick={()=>setDisplay(true)} className="scratch-card-img" src={cards[num].src}/>
                <div className={`scratch-content ${cards[num].position} ${display?'show-card':'hide-card'}`}>
                    {result.table(cards[num].size)}
                </div>
            </div>
            <div>
                Winnings so far: P20
            </div>
        </div>
    )
 */