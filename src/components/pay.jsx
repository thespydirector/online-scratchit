import React, {useEffect,useContext, useState, useRef} from 'react';
import { FirebaseContext } from '../contexts/firebase';

import tip1 from '../assets/tip-1.PNG';
import tip2 from '../assets/tip-2.png';
import tip3 from '../assets/tip-3.png';
import gcash from '../assets/gcash.png';
import back from '../assets/back.svg';

import Loading from './loading.jsx';

import "../styles/all.css";

function Pay({totalPrice, gamesList,showPay}){

    const [loading,setLoading]=useState(false);
    const [image,setImage]=useState(null);

    useEffect(()=>{
        window.scrollTo(0,0);
    },[])

    const fileUpload = useRef(null);

    const boughtGames=gamesList.filter(e=>e.qty>0);
    const orderSummary = boughtGames.map((element)=>
                <tr key={element.name} className="order-row">
                    <td>{element.name}</td>
                    <td>{element.qty}</td>
                    <td>₱{parseInt(element.price.substring(1)*element.qty)}.00</td>
                </tr>
        )

    const onButtonClick = () => {
        fileUpload.current.click();
    };

    const onImgUpload =()=>{
        console.log("R");
        setImage(fileUpload.current.files[0]);  
        setLoading(true);
        //ParseImage(fileUpload.current.files[0]);
        //setLoading(false);
    }

if(loading===true){
    return(
        <Loading image={image} gamesList={gamesList} totalPrice={totalPrice} showPlay={value=>showPay(value)} status={status=>setLoading(status)}/>
    )
}

else{
    return(
    <div className="pay-wrapper">
        <div className="pay-back" onClick={()=>showPay(1.2)}><img src={back}/> BACK</div>
        <article className="pay-order-summary-wrapper">
            <header id="pay">
                Order Summary
            </header>
            <section>
                <table>
                    <thead>
                        <tr>
                            <th>Card</th>
                            <th>Qty</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        {orderSummary}
                        <tr className="order-row">
                            <td>Minus: Wallet Credits</td>
                            <td>-</td>
                            <td>₱0.00</td>
                        </tr>
                        <tr className="final-row">
                            <td>Grand Total</td>
                            <td></td>
                            <td>₱{totalPrice}.00</td>
                        </tr>
                    </tbody>
                </table>
            </section>
        </article>
        <article>
            <input type="file" hidden ref={fileUpload} onChange={onImgUpload}/>
            <div role="button" className="pay-button" onClick={onButtonClick} >
                <img id="gcash-pay" src={gcash}/><span>UPLOAD GCASH RECEIPT HERE</span>
            </div>
        </article>
        <article className="pay-tips">
            <header>
                Pay In Three Easy Steps!
            </header>
            <section>
                <ol>
                    <li>
                        Transfer <strong>₱{totalPrice}.00</strong> to <strong>09176217025</strong> using <strong>GCash Express Send</strong>.
                        <img src={tip1}/>
                    </li>
                    <li>
                        Download the <strong>whole</strong> receipt and upload <a href="#pay">above</a>.
                        <img src={tip2}/>
                    </li>
                    <li>
                        Wait a few seconds for processing to finish.
                        <img src={tip3}/>
                    </li>      
                </ol>
            </section>
        </article>
    </div>
    )
}
}

export default Pay;