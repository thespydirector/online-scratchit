//For play area navbar
import pic from '../assets/trial2.png'
import menu from '../assets/menu.svg';
import close from '../assets/close.svg';
import '../styles/playnav.css';
import '../styles/landing.css';
import { useEffect, useState } from 'react';
import {Switch,Route,Redirect, Link} from 'react-router-dom';

function PlayNav(){
    let date = Date.now();
    let dateObj = new Date(date);

    const [burger, setBurger]=useState(false);

    useEffect(()=>{
        if(burger===true){
            document.querySelector("body").style.overflow = 'hidden';
        }

        else if (burger===false){
            document.querySelector("body").style.overflow = 'scroll';
        }

    },[burger])

    return(
        <div className="play-nav">
            <div onClick={()=>setBurger(!burger)} className="play-nav-burger">
                {burger?<embed id="nav-close" src={close}/>:<embed id="nav-menu" src={menu}/>}
            </div>
            <div className="play-nav-logo">
                {<img id="nav-logo" alt="title" src={pic}/>}
            </div>
            <div className="play-nav-logout">
                <div className="play-nav-logout-btn">
                    SIGNOUT
                </div>
            </div>
            {burger?<Burger burger={burger}/>:<div></div>}
        </div>
    )
}

function Burger({burger}){
    return(
        <div className="play-nav-burger-wrapper">
            <div className="sidebar-container">
                <div className="sidebar-name">
                    Hello Raver!
                </div>
                <div className="sidebar-wallet">
                    <div className="sidebar-wallet-label">
                        Wallet Balance
                    </div>
                    <div className="sidebar-wallet-balance">
                        P20.00
                    </div>
                </div>
                <div className="sidebar-actions">
                    <div className="sidebar-actions-withdraw">
                        Withdraw
                    </div>
                    <div className="sidebar-actions-load">
                        Load Now
                    </div>
                </div>
                <div className="sidebar-menu">
                    <div >
                        <svg width="36" height="36" viewBox="0 0 36 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M18 0C8.064 0 0 8.064 0 18C0 27.936 8.064 36 18 36C27.936 36 36 27.936 36 18C36 8.064 27.936 0 18 0ZM18 32.4C10.062 32.4 3.6 25.938 3.6 18C3.6 10.062 10.062 3.6 18 3.6C25.938 3.6 32.4 10.062 32.4 18C32.4 25.938 25.938 32.4 18 32.4ZM25.2 18L14.4 26.1V9.9L25.2 18Z" fill="#F8F9FF"/>
                        </svg>
                        <div className="sidebar-menu-item-label">
                            <Link style={{color:"white",textDecoration:"none"}} to="/play">Play Here</Link>
                        </div>
                    </div>
                    <div >
                        <svg width="36" height="36" viewBox="0 0 42 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M6 18C6 8.06 14.06 0 24 0C33.94 0 42 8.06 42 18C42 27.94 33.94 36 24 36C19.02 36 14.54 33.98 11.28 30.72L14.12 27.88C16.64 30.42 20.14 32 24 32C31.74 32 38 25.74 38 18C38 10.26 31.74 4 24 4C16.26 4 10 10.26 10 18H16L8 25.98L0 18H6ZM22 20V10H25V18.3L32.04 22.48L30.5 25.04L22 20Z" fill="#F8F9FF"/>
                        </svg>
                        <div className="sidebar-menu-item-label">
                            <Link style={{color:"white",textDecoration:"none"}} to="/bets">Transaction History</Link>
                        </div>
                    </div>
                    <div>
                        <svg width="36" height="36" viewBox="0 0 27 36" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.0909 0V4.90909C20.3236 4.90909 26.1818 10.7673 26.1818 18C26.1818 20.5691 25.4291 22.9582 24.1527 24.9709L21.7636 22.5818C22.5 21.2236 22.9091 19.6527 22.9091 18C22.9091 12.5836 18.5073 8.18182 13.0909 8.18182V13.0909L6.54545 6.54545L13.0909 0ZM3.27273 18C3.27273 23.4163 7.67455 27.8182 13.0909 27.8182V22.9091L19.6364 29.4545L13.0909 36V31.0909C5.85818 31.0909 0 25.2327 0 18C0 15.4309 0.752727 13.0418 2.02909 11.0291L4.41818 13.4182C3.68182 14.7763 3.27273 16.3473 3.27273 18Z" fill="#F8F9FF"/>
                        </svg>
                        <div className="sidebar-menu-item-label">
                            <Link style={{color:"white",textDecoration:"none"}} to="/recurring">Payout History</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PlayNav;